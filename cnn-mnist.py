'''
Created on Apr 14, 2019

@author: jerron
'''
import numpy as np
from keras.models import Sequential,load_model
from keras.layers import Activation, Dense, Dropout
from keras.layers import Conv2D, MaxPooling2D, Flatten
from keras.utils import to_categorical, plot_model
from keras.datasets import mnist
import os.path
import matplotlib.pyplot as plt

# load mnist dataset
(x_train, y_train), (x_test, y_test) = mnist.load_data()
# compute the number of labels
num_labels = len(np.unique(y_train))
# convert to one-hot vector
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)
# input image dimensions
image_size = x_train.shape[1]
# resize and normalize
ox_test,oy_test=x_test,y_test

x_train = np.reshape(x_train, [-1, image_size, image_size, 1])
x_test = np.reshape(x_test, [-1, image_size, image_size, 1])
x_train = x_train.astype('float32') / 255
x_test = x_test.astype('float32') / 255

model_file='cnn-mnist'
if os.path.isfile(model_file):
    model=load_model(model_file)
else:
    
    # network parameters
    # image is processed as is (square grayscale)
    input_shape = (image_size, image_size, 1)
    batch_size = 128
    kernel_size = 3
    pool_size = 2
    filters = 64
    dropout = 0.2
    # model is a stack of CNN-ReLU-MaxPooling
    model = Sequential()
    model.add(Conv2D(filters=filters,
                     kernel_size=kernel_size,
                     activation='relu',
                     input_shape=input_shape))
    model.add(MaxPooling2D(pool_size))
    model.add(Conv2D(filters=filters,
                     kernel_size=kernel_size,
                     activation='relu'))
    model.add(MaxPooling2D(pool_size))
    model.add(Conv2D(filters=filters,
                     kernel_size=kernel_size,
                     activation='relu'))
    model.add(Flatten())
    # dropout added as regularizer
    model.add(Dropout(dropout))
    # output layer is 10-dim one-hot vector
    model.add(Dense(num_labels))
    model.add(Activation('softmax'))
    model.summary()
    plot_model(model, to_file='cnn-mnist.png', show_shapes=True)
    # loss function for one-hot vector
    # use of adam optimizer
    # accuracy is good metric for classification tasks
    model.compile(loss='categorical_crossentropy',
    optimizer='adam',
    metrics=['accuracy'])
    # train the network
    model.fit(x_train, y_train, epochs=10, batch_size=batch_size)
    loss, acc = model.evaluate(x_test, y_test, batch_size=batch_size)
    model.save(model_file,)
    print("\nTest accuracy: %.1f%%" % (100.0 * acc))

predictions = model.predict(x_test)
plt.figure(figsize=(8,8))
j=1
for i in range(len(predictions)):
    score=np.dot(oy_test[i],predictions[i].T)
    if score<.5:
        plt.subplot(8,8, j)
        image = ox_test[i]
        plt.imshow(image, cmap='gray')
        plt.axis('off')
        o=0
        p=0
        for k in range(len(predictions[i])):
            if predictions[i][k]>predictions[i][p]:
                p=k
            if oy_test[i][k]>oy_test[i][o]:
                o=k
        plt.title(p)
        print(j,o,p,score)
        j+=1
        if j>64:
            break
            
plt.show()


print("end")


