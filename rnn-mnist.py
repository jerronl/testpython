'''
Created on Apr 14, 2019

@author: jerron
'''
import numpy as np
from keras.models import Sequential,load_model
from keras.layers import Dense, Activation, SimpleRNN,LSTM
from keras.utils import to_categorical, plot_model
from keras.datasets import mnist
import os.path
import matplotlib.pyplot as plt


# load mnist dataset
(x_train, y_train), (x_test, y_test) = mnist.load_data()
# compute the number of labels
num_labels = len(np.unique(y_train))
# convert to one-hot vector
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)
ox_test,oy_test=x_test,y_test
# resize and normalize
image_size = x_train.shape[1]
x_train = np.reshape(x_train,[-1, image_size, image_size])
x_test = np.reshape(x_test,[-1, image_size, image_size])
x_train = x_train.astype('float32') / 255
x_test = x_test.astype('float32') / 255

model_file='rnn-mnist'
if os.path.isfile(model_file):
    model=load_model(model_file)
    model.summary()
else:
    # network parameters
    input_shape = (image_size, image_size)
    batch_size = 128
    units = 256
    dropout = 0.2
    # model is RNN with 256 units, input is 28-dim vector 28 timesteps
    model = Sequential()
    model.add(LSTM(units=units,
                        dropout=dropout,
                        input_shape=input_shape))
    model.add(Dense(num_labels))
    model.add(Activation('softmax'))
    model.summary()
    plot_model(model, to_file='rnn-mnist.png', show_shapes=True)
    # loss function for one-hot vector
    # use of sgd optimizer
    # accuracy is good metric for classification tasks
    model.compile(loss='categorical_crossentropy',
    optimizer='sgd',
    metrics=['accuracy'])
    # train the network
    model.fit(x_train, y_train, epochs=20, batch_size=batch_size)
    loss, acc = model.evaluate(x_test, y_test, batch_size=batch_size)
    print("\nTest accuracy: %.1f%%" % (100.0 * acc))
    model.save(model_file)
    
predictions = model.predict(x_test)
plt.figure(figsize=(8,8))
j=1
for i in range(len(predictions)):
    score=np.dot(oy_test[i],predictions[i].T)
    if score<.5:
        plt.subplot(8,8, j)
        image = ox_test[i]
        plt.imshow(image, cmap='gray')
        plt.axis('off')
        o=0
        p=0
        for k in range(len(predictions[i])):
            if predictions[i][k]>predictions[i][p]:
                p=k
            if oy_test[i][k]>oy_test[i][o]:
                o=k
        plt.title(p)
        print(j,o,p,score)
        j+=1
        if j>64:
            break
            


print("end")
